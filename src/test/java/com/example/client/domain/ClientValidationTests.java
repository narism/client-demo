package com.example.client.domain;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ClientValidationTests {

    private static Validator validator;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void invalidEmailNotAllowed() {
        Client client = Client.builder()
                .address("address")
                .username("username")
                .lastName("lastname")
                .firstName("firstName")
                .country("FI")
                .email("invalid")
                .build();
        Set<ConstraintViolation<Client>> violations = validator.validate(client);
        assertThat(violations.size()).isEqualTo(1);
        ConstraintViolationImpl constraintViolation = (ConstraintViolationImpl) violations.stream().findFirst().get();
        assertThat(constraintViolation.getMessage()).isEqualTo("must be a well-formed email address");
    }

    @Test
    public void correctDataPassesValidation() {
        Client client = Client.builder()
                .address("address")
                .username("username")
                .lastName("lastname")
                .firstName("firstName")
                .country("FI")
                .email("test@test.ee")
                .build();
        Set<ConstraintViolation<Client>> violations = validator.validate(client);
        assertThat(violations.size()).isEqualTo(0);
    }
}
