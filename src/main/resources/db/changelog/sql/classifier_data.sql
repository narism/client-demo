INSERT INTO classifier (cf_code, cf_name, cf_value, created_by, created_time, modified_by, modified_time)
VALUES ('COUNTRY', 'Finland', 'FI', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP),
       ('COUNTRY', 'Estonia', 'EE', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP),
       ('COUNTRY', 'Sweden', 'SW', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP),
       ('COUNTRY', 'Latvia', 'LV', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP),
       ('COUNTRY', 'Lithuania', 'LT', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP);
