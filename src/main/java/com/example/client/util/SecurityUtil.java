package com.example.client.util;

import com.example.client.service.security.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

public final class SecurityUtil {

    private SecurityUtil() {
    }

    public static CustomUserDetails getAuthenticatedUser() {
        return (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static Long getCurrentUserId() {
        return getAuthenticatedUser().getUserId();
    }
}
