package com.example.client.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@Data
@Entity(name = "app_user")
public class AppUserEntity {
    @Id
    private Long userId;
    private String username;
    private String password;
    private String status;
    private Instant createdTime;
    private String createdBy;
    private Instant modifiedTime;
    private String modifiedBy;
}