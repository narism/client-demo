package com.example.client.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

import static com.example.client.util.SecurityUtil.getAuthenticatedUser;

@Data
@Entity(name = "client")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long clientId;
    private Long userId;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String address;
    private String country;
    private Instant createdTime;
    private String createdBy;
    private Instant modifiedTime;
    private String modifiedBy;

    public ClientEntity mergeData(ClientEntity fromEntity) {
        this.firstName = fromEntity.firstName;
        this.lastName = fromEntity.lastName;
        this.username = fromEntity.username;
        this.email = fromEntity.email;
        this.address = fromEntity.address;
        this.country = fromEntity.country;
        return this;
    }

    @PrePersist
    public void prePersist() {
        createdTime = Instant.now();
        createdBy = getAuthenticatedUser().getUsername();
        modifiedTime = Instant.now();
        modifiedBy = getAuthenticatedUser().getUsername();
    }

    @PreUpdate
    public void preUpdate() {
        modifiedTime = Instant.now();
        modifiedBy = getAuthenticatedUser().getUsername();
    }
}