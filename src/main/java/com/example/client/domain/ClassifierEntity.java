package com.example.client.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@Data
@Entity(name = "classifier")
public class ClassifierEntity {
    @Id
    private Long clfiId;
    private String cfCode;
    private String cfName;
    private String cfValue;
    private Instant createdTime;
    private String createdBy;
    private Instant modifiedTime;
    private String modifiedBy;
}