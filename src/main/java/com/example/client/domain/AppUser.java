package com.example.client.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
public class AppUser {
    private Long userId;
    private String username;
    private String password;
    private String status;

    public static Optional<AppUser> fromEntity(Optional<AppUserEntity> entity) {
        return entity.map(AppUser::toAppUserDto);
    }

    private static AppUser toAppUserDto(AppUserEntity entity) {
        return AppUser.builder()
                .password(entity.getPassword())
                .userId(entity.getUserId())
                .username(entity.getUsername())
                .status(entity.getStatus())
                .build();
    }
}
