package com.example.client.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Classifier {
    private String cfCode;
    private String cfName;
    private String cfValue;

    public static Classifier fromEntity(ClassifierEntity entity) {
        return toClientDto(entity);
    }

    private static Classifier toClientDto(ClassifierEntity entity) {
        return Classifier.builder()
                .cfCode(entity.getCfCode())
                .cfName(entity.getCfName())
                .cfValue(entity.getCfValue())
                .build();
    }
}
