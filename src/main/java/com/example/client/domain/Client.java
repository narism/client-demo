package com.example.client.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Optional;

import static com.example.client.util.SecurityUtil.getCurrentUserId;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    private Long clientId;
    @NotBlank
    @Size(min = 2, max = 50)
    private String firstName;
    @NotBlank
    @Size(min = 2, max = 50)
    private String lastName;
    @NotBlank
    @Size(min = 2, max = 50)
    private String username;
    @Email
    @Size(min = 4, max = 60)
    private String email;
    @NotBlank
    @Size(min = 1, max = 255)
    private String address;
    @NotBlank
    @Size(min = 1, max = 60)
    private String country;

    public static Optional<Client> fromEntity(Optional<ClientEntity> entity) {
        return entity.map(Client::toClientDto);
    }

    public static Client fromEntity(ClientEntity entity) {
        return toClientDto(entity);
    }

    private static Client toClientDto(ClientEntity entity) {
        return Client.builder()
                .clientId(entity.getClientId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .username(entity.getUsername())
                .email(entity.getEmail())
                .address(entity.getAddress())
                .country(entity.getCountry())
                .build();
    }

    public ClientEntity toEntity() {
        return ClientEntity
                .builder()
                .clientId(clientId)
                .userId(getCurrentUserId())
                .firstName(firstName)
                .lastName(lastName)
                .address(address)
                .username(username)
                .country(country)
                .email(email)
                .build();

    }
}
