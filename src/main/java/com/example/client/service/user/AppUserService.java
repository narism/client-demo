package com.example.client.service.user;

import com.example.client.domain.AppUser;
import com.example.client.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AppUserService {

    private final AppUserRepository appUserRepository;

    public Optional<AppUser> findByUsername(String username) {
        return AppUser.fromEntity(appUserRepository.findByUsername(username));
    }
}
