package com.example.client.service.security;

import com.example.client.domain.AppUser;
import com.example.client.service.user.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final AppUserService appUserService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        AppUser user = appUserService.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException(username)
        );
        return new CustomUserDetails(user);
    }
}
