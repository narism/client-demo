package com.example.client.service.client;

import com.example.client.domain.Client;
import com.example.client.domain.ClientEntity;
import com.example.client.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.client.util.SecurityUtil.getCurrentUserId;
import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public List<Client> findAllByUserId(Long userId) {
        return clientRepository.findAllByUserId(userId).stream().map(Client::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public void persistClient(Client client) {
        ClientEntity currentEntity = client.toEntity();
        if (isNull(client.getClientId())) {
            clientRepository.save(currentEntity);
        } else {
            ClientEntity clientEntity = clientRepository.getOne(client.getClientId());
            if (!clientEntity.getUserId().equals(currentEntity.getUserId())) {
                throw new RuntimeException("User trying to modify invalid client");
            }
            clientRepository.save(clientEntity.mergeData(currentEntity));
        }
    }

    public Optional<Client> findClientByClientId(Long id) {
        return Client.fromEntity(clientRepository.findByClientIdAndUserId(id, getCurrentUserId()));
    }
}
