package com.example.client.service.classifier;

import com.example.client.domain.Classifier;
import com.example.client.repository.ClassifierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClassifierService {

    private final ClassifierRepository classifierRepository;

    public List<Classifier> findAllByCode(String code) {
        return classifierRepository.findAllByCfCode(code).stream().map(Classifier::fromEntity).collect(Collectors.toList());
    }
}
