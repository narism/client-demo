package com.example.client.repository;

import com.example.client.domain.ClassifierEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassifierRepository extends JpaRepository<ClassifierEntity, Long> {
    List<ClassifierEntity> findAllByCfCode(String code);
}
