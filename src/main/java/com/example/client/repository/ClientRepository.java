package com.example.client.repository;

import com.example.client.domain.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    List<ClientEntity> findAllByUserId(Long userId);

    Optional<ClientEntity> findByClientIdAndUserId(Long id, Long currentUserId);
}
