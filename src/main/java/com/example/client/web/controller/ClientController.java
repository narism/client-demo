package com.example.client.web.controller;

import com.example.client.domain.Client;
import com.example.client.service.classifier.ClassifierService;
import com.example.client.service.client.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Optional;

import static com.example.client.util.SecurityUtil.getAuthenticatedUser;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ClientController {

    public static final String COUNTRIES = "countries";
    public static final String CLIENT = "client";
    public static final String COUNTRY_CLASSIFIER = "COUNTRY";
    private final ClientService clientService;
    private final ClassifierService classifierService;

    @GetMapping
    public String getAllCurrentUsersClients(Model model) {
        log.debug("Finding all clients for user");
        model.addAttribute("clients", clientService.findAllByUserId(getAuthenticatedUser()
                .getUserId()));
        return "index";
    }

    @GetMapping("clients")
    public String getNewClient(Model model) {
        model.addAttribute(COUNTRIES, classifierService.findAllByCode(COUNTRY_CLASSIFIER));
        model.addAttribute(CLIENT, new Client());
        return CLIENT;
    }

    @PostMapping("clients")
    public String addClientData(@Valid @ModelAttribute Client client, BindingResult result, Model model) {
        log.debug("Client data modifications received");
        if (result.hasErrors()) {
            log.debug("Input has {} validation errors", result.getErrorCount());
            model.addAttribute(COUNTRIES, classifierService.findAllByCode(COUNTRY_CLASSIFIER));
            return CLIENT;
        }
        clientService.persistClient(client);
        return "redirect:";
    }

    @GetMapping("clients/{id}")
    public String getAllCurrentUsersClients(@PathVariable("id") Long id, Model model) {
        model.addAttribute(COUNTRIES, classifierService.findAllByCode(COUNTRY_CLASSIFIER));
        Optional<Client> client = clientService.findClientByClientId(id);
        if (!client.isPresent()) {
            model.addAttribute("error", "User has no such client with provided ID");
        }
        model.addAttribute(CLIENT, client);
        return CLIENT;
    }
}
