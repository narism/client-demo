Client demo app

Prerequisites

JAVA 11+

Running the application clone from https://narism@bitbucket.org/narism/client-demo.git

In project root

gradlew clean build

navigate to build/libs

java -jar client.jar

application should be running in localhost:8080

dev users are 
user1:password1
user2:password2
user3:password3